<?php

namespace Database\Factories;

use App\Models\Package;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PackageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Package::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'intro' => "<p>".implode("</p>\n\n<p>", $this->faker->paragraphs(rand(3,6)))."</p>",
            'description' => "<p>".implode("</p>\n\n<p>", $this->faker->paragraphs(rand(9,12)))."</p>",
            'price' => $this->faker->randomFloat(2, 20, 100),
        ];
    }

}
