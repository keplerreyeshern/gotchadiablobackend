<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'intro' => "<p>".implode("</p>\n\n<p>", $this->faker->paragraphs(rand(3,6)))."</p>",
            'description' => "<p>".implode("</p>\n\n<p>", $this->faker->paragraphs(rand(9,12)))."</p>",
            'date' => $this->faker->dateTime(),
        ];
    }
}
