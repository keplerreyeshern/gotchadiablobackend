<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accountant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ComponentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $categories = Category::where('active', true)->whereNull('parent_id')->get();
//        $result = [
//            'categories' => $categories,
//        ];
//        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function click($type)
    {
        $accountant = new Accountant();
        $accountant->month = date("m");
        $accountant->type = $type;
        $accountant->save();
        return $accountant;
    }
}
