<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\ContactEmail;
use App\Models\Event;
use App\Models\Image;
use App\Models\Message;
use App\Models\Package;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        $packages = Package::get();
        $events = Event::get();
        $images = Image::get();
        $reservations = Reservation::get();
        $result = [
            'users' => $users,
            'packages' => $packages,
            'events' => $events,
            'images' => $images,
            'reservations' => $reservations,
        ];
        return $result;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {
        $for = "torosoprano@hotmail.com.mx";
//        $for = "keplerreyeshern@gmail.com";

        $data = new \stdClass();
        $data->name = $request['name'];
        $data->telephone = $request['telephone'];
        $data->email = $request['email'];
        $data->message = $request['message'];

        Mail::to($for)->send(new ContactEmail($data));
        $message = new Message();
        $message->name = $request['name'];
        $message->email = $request['email'];
        $message->telephone = $request['telephone'];
        $message->message = $request['message'];
        $message->save();
        return $message;
    }
}
