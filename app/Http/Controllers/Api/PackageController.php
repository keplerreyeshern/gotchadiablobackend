<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str as Str;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pack = new Package();
        $pack->title = $request['title'];
        $pack->slug = Str::slug($request['title']);
        $pack->intro = $request['intro'];
        $pack->price = $request['price'];
        $pack->description = $request['description'];
        $pack->active = true;
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/packages/' . $name,  \File::get($file));
            $pack->image = '/storage/images/packages/' . $name;
        }
        $pack->save();
        return $pack;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        if($package->active){
            $package->active = false;
        } else {
            $package->active = true;
        }

        $package->save();
        return $package;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $pack)
    {
        $pack->title = $request['title'];
        $pack->slug = Str::slug($request['title']);
        $pack->intro = $request['intro'];
        $pack->price = $request['price'];
        $pack->description = $request['description'];
        $pack->active = true;
        $file = $request->file('image');
        if ($file){
            $name =  time()."_".$file->getClientOriginalName();
            Storage::disk('public')->put('/images/packages/' . $name,  \File::get($file));
            $pack->image = '/storage/images/packages/' . $name;
        }
        $pack->save();
        return $pack;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return $package;
    }
}
