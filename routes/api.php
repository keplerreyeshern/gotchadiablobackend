<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ComponentsController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\MainController;
use App\Http\Controllers\Api\PackageController;
use App\Http\Controllers\Api\PasswordResetController;
use App\Http\Controllers\Api\ReservationController;
use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/home', [MainController::class, 'index']);
Route::get('/home/{news}', [MainController::class, 'show']);
Route::post('/contact', [MainController::class, 'contact']);
Route::get('/release/{release}', [MainController::class, 'release']);

Route::get('/components', [ComponentsController::class, 'index']);
Route::get('/components/clicks/{type}', [ComponentsController::class, 'click']);

Route::get('/password/find/{token}', [PasswordResetController::class, 'find']);
Route::post('/password/create', [PasswordResetController::class, 'create']);
Route::post('/password/reset', [PasswordResetController::class, 'reset']);

Route::group(['middleware' => ['auth:api']], function(){

    // Users
    Route::resource('/users', UserController::class);

    // Reservations
    Route::resource('/reservations', ReservationController::class);

    // Packages
    Route::resource('/packages', PackageController::class);
    Route::post('/packages/{pack}', [PackageController::class, 'update']);

    // Events
    Route::resource('/events', EventController::class);
    Route::post('/events/{event}', [EventController::class, 'update']);

    // Gallery
    Route::resource('/gallery', GalleryController::class);
    Route::post('/gallery/{image}', [GalleryController::class, 'update']);
});
